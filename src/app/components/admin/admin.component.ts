import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  selectedFile=null;
  constructor(private authService: AuthService, private http: HttpClient) { }

  ngOnInit(): void {
  }

  getUserList(){
    this.authService.getUsers().subscribe(data=>{
      console.log(data);
    })
  }

  onFileSelected(event:any){
    this.selectedFile=event.target.files[0]
    console.log(event)
  }

  
}
